gcc:
	g++ source.cpp -lreadline -lncurses -o todo
	./todo todo

clang:
	clang++ source.cpp -lreadline -lncurses -o todo
	./todo todo

intel:
	icpc source.cpp -lreadline -lncurses -o todo
	./todo todo
