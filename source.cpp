#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <readline/readline.h>
#include <readline/history.h>
#include <filesystem>
#include <ncurses.h>

std::ofstream output;
std::ifstream input;

//   clang does not work well with std::filesystem

namespace fs = std::filesystem;

void read(std::string file, std::string &content);
void recalc(std::string &content);
void write(std::string file, std::string content);
std::string rm(std::string file, int rmopt);
void mark(std::string buffer, std::string &content);
void unmark(std::string &content, int lnum);
void setparser(std::string in, std::string &content);
void priority(int optnum, int priority, std::string &content);
int lines(std::string content);
void edit(std::string &content, std::string buf);

int main(int argc, char** argv){
    std::string homedir = getenv("HOME"), location, command;
    std::string fileloc = homedir + "/.local/share/todo/";
    std::string content = "", foo, bar;
    int lnum;
    int tempint;
    unsigned long int* templong;
//   clang does not work well with std::filesystem

    fs::create_directories(fileloc);

    if(argv[1] == nullptr){
        std::cout << "usage:\ntodo [filename]\nopens or creates a list\n";
        return 0;
    }
    std::string file = fileloc + std::string(argv[1]);
    output.open(file, std::ios::app);
    output.close();
    char* buf;
    std::string tempbuf;
    while ((buf = readline(">> ")) != nullptr) {
        if (strlen(buf) > 0) {
            add_history(buf);
        }
        if (std::string(buf).substr(0,5) == "print"){
            read(file, content);
            std::cout << content;
        }
        else if (std::string(buf).substr(0,4) == "help"){
            std::cout << "command usage:\n[add <string>] to add an option to the list\n";
            std::cout << "[rm <number>] to remove an option from the list\n";
            std::cout << "[mark <number> <string>] to mark an option in the list\n";
            std::cout << "[rmmark <number>] to remove remove a mark form an option in the list\n";
            std::cout << "[chlist <string>] to change or creates the list\n";
            std::cout << "[print] to print the list\n";
            std::cout << "[help] to print this\n";
            std::cout << "[quit] self explanatory\n";
            std::cout << "[set <number> priority=<number>] to push an option to a certain spot in the list (NOTE: Only works while using it to push it higher in the list, trying to push it lower will result in no changes)\n";
        }
        else if (std::string(buf).substr(0,4) == "add "){
            tempbuf = buf;
            tempbuf.replace(0, 4, "");
            read(file, content);
            content += tempbuf + "\n";
            recalc(content);
            write(file, content);
        }
        else if (std::string(buf).substr(0,7) == "rmmark "){
            tempbuf = buf;
            tempbuf.replace(0, 7, "");
            std::stringstream stream(tempbuf);
            stream >> lnum;
            read(file, content);
            unmark(content, lnum);
            recalc(content);
            write(file, content);
        }
        else if (std::string(buf).substr(0,3) == "rm "){
            tempbuf = buf;
            tempbuf.replace(0, 3, "");
            int n = std::stoi(tempbuf);
            content = rm(file, n);
            recalc(content);
            write(file, content);
        }
        else if (std::string(buf).substr(0,5) == "mark "){
            tempbuf = buf;
            tempbuf.replace(0, 5, "");
            read(file, content);
            mark(tempbuf, content);
            recalc(content);
            write(file, content);
        }
        else if (std::string(buf).substr(0,7) == "chlist "){
            tempbuf = buf;
            tempbuf.replace(0, 7, "");
            file = fileloc + tempbuf;
            std::cout << file << std::endl;
            output.open(file, std::ios::app);
            output.close();
        }      
        else if (std::string(buf).substr(0, 4) == "set "){
            tempbuf = buf;
            tempbuf.replace(0, 4, "");
            read(file, content);
            setparser(tempbuf, content);
            recalc(content);
            write(file, content);
        }
        else if (std::string(buf).substr(0, 5) == "edit "){
            tempbuf = buf;
            tempbuf.replace(0, 5, "");
            read(file, content);
            edit(content, tempbuf);
            recalc(content);
            write(file, content);
        }  
        else if (std::string(buf).substr(0, 4) == "quit"){
            return 0;
        }
        free(buf);
        content = "";
    }
}

int lines(std::string content){
    std::istringstream stream(content);
    std::string foo;
    int i = 1;
    while(getline(stream, foo)){
        i++;
    }
    return i;
}

void read(std::string file, std::string &content){
    std::string foo;
    input.open(file);
    while(!input.eof()){
        getline(input, foo);
        content += foo + "\n";
    }
    input.close();
}

void write(std::string file, std::string content){
    output.open(file);
    output << content;
    output.close();
}

std::string rm(std::string file, int rmopt){
    std::string foo;
    int i = 1;
    std::string out = "";
    input.open(file);
    while(!input.eof()){
        getline(input, foo);
        if(i != rmopt) out += foo + "\n";
        i++;
    }
    input.close();
    return out;
}

void recalc(std::string &content){
    std::istringstream stream(content);
    std::string foo,bar = "";
    int i = 1;
    while(getline(stream, foo)){
        while(foo[0] >= '0' && foo[0] <= '9'){
            foo.replace(0,1, "");
        }
        while(foo[0] == '.' || foo[0] == ' '){
            foo.replace(0,1, "");
        }
        if(foo != ""){
            bar += std::to_string(i) + ". " + foo + "\n";
            i++;
        }
    }
    content = bar;
}
void mark(std::string buffer, std::string &content){
    std::istringstream stream(buffer);
    std::istringstream cstream(content);
    int lnum, i = 1, a = 0;
    std::string foo, bar = "";
    std::string marking;
    stream >> lnum;
    getline(stream, marking);
    marking.replace(0, 1, "");
    // std::cout << lnum << std::endl << marking;
    while(getline(cstream, foo)){
        if(i == lnum){
            while(foo[0] >= '0' && foo[0] <= '9'){
                foo.replace(0,1, "");
            }
            while(foo[0] == '.' || foo[0] == ' '){
                foo.replace(0,1, "");
            }
            foo = "<" + marking + ">" + foo;
        }
        bar += foo + "\n";
        i++;
    }  
    content = bar;  
}

void unmark(std::string &content, int lnum){
    std::istringstream cstream(content);
    int i = 1, a = 0;
    std::string foo, bar = "";
    // std::cout << lnum << std::endl << marking;
    while(getline(cstream, foo)){
        if(i == lnum){            // std::cout << content;
            
            while(foo[0] == '.' || foo[0] == ' '){
                foo.replace(0,1, "");
            }
            if(foo[0] == '<'){
                foo.replace(0,1, "");
                while(foo[0] != '>'){
                    foo.replace(0,1, "");
                    // std::cout << "a\n";
                }
                foo.replace(0,1, "");
            }
            // std::cout << "a\n";
        }
        bar += foo + "\n";
        i++;
    }  
    content = bar;  
}

void setparser(std::string in, std::string &content){
    std::stringstream stream;
    stream << in;
    int optnum, prioritynum;
    stream >> optnum;
    // std::cout << num;
    std::string opts;
    stream >> opts;
    std::stringstream stream2;
    if(opts.substr(0, 9) == "priority="){
        opts.replace(0, 9, "");
        stream2 << opts;
        stream2 >> prioritynum;
    }
    // std::cout << optnum << "\n" << prioritynum << "\n";
    priority(optnum, prioritynum, content);
}

void priority(int optnum, int prioritynum, std::string &content){ //only works when moving options up the list, otherwise it just creates a gap between options, which is corrected by recalc
    int clines = lines(content);
    std::string bar[clines];
    std::istringstream stream(content);
    std::string foo;
    bool shiftback = 0;
    int i = 0;
    while(getline(stream, foo)){
        // std::cout << "\ncycle\n";
        if(i == prioritynum-1){
            i++;
            // std::cout << "shift 1 success \n";
            shiftback = 1;
            bar[i] = foo;
        }
        else if(i == optnum && shiftback == 1){
            bar[prioritynum-1] = foo;
            i--;
            // std::cout << "shift 2 success \n";
            shiftback = 0;
        }
        else bar[i] = foo;
        
        i++;
    }
    content = "";
    for(i = 0; i < clines; i++){
        content += bar[i] + "\n";
    }
}

void edit(std::string &content, std::string buf){
    std::stringstream stream;
    std::stringstream str;
    std::string tline,  line;
    str << content;
    stream << buf;
    int optnum;
    stream >> optnum;
    // std::cout << optnum;
    std::string tempcontent = content;
    int i = 0;
    int lnum = lines(content);
    while(getline(str, tline)){
        if(i == optnum - 1) line = tline;
        i++;
    }
    // std::cout << line << std::endl;
    initscr();			/* Start curses mode 		  */
	printw(line.c_str());	/* Print Hello World		  */
	refresh();			/* Print it on to the real screen */
	getch();			/* Wait for user input */
	endwin();			/* End curses mode		  */
}